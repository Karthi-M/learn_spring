package com.example.springdemo;

public class MyApp {

	public static void main(String[] args) {
		
//		Creating an Object
		Coach theCoach = new TrackCoach(); // TrackCoach is Hard coded
		
//		Using this Object
		System.out.println("Getting Daily Workout of a Coach : " + theCoach.getDailyWorkout());

	}

}
