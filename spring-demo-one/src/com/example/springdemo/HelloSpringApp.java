package com.example.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringApp {

	public static void main(String[] args) {

//		Load Spring Configuration File
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
//		Retrieve bean from Spring Container
		Coach theCoach = context.getBean("myCoach2", Coach.class);
		
//		Calling methods on the bean
		System.out.println(theCoach.getDailyWorkout());
		
//		Closing the Context
		context.close();
	}

}
